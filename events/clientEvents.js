const messageHandler = require('../handler/messageHandler');
/**
 * Events from discord are registered here
 *
 * @param {import('discord.js').Client} client
 */
module.exports = async (client) => {
  if (!client) {
    return false;
  }

  client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
  });

  client.on('message', messageHandler);

  return true;
};
