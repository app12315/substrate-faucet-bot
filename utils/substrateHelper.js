const { ApiPromise, WsProvider, Keyring } = require('@polkadot/api');
const crypto = require('@polkadot/util-crypto');
const { BN } = require('bn.js');

module.exports = {
  /**
   * Get Sender account from mnemonic
   */
  getSender: () => {
    return new Keyring({ type: 'sr25519' }).addFromMnemonic(
      process.env.mnemonic,
    );
  },

  /**
   * Connect to node and retun an new api instance
   */
  connectToNode: async () => {
    const provider = new WsProvider(process.env.nodeUrl);
    const api = await ApiPromise.create({
      provider,
      types: {
        Address: 'IndicesLookupSource',
        LookupSource: 'IndicesLookupSource',
        Nonce: 'u64',
        EventMessage: 'Vec<u8>',
        Campaign: {
          id: 'Hash',
          manager: 'AccountId',
          deposit: 'Balance',
          expiry: 'BlockNumber',
          cap: 'Balance',
          name: 'Vec<u8>',
          protocol: 'u8',
          status: 'u8',
        },
        Treasury: {
          id: 'Hash',
          campaign_id: 'Hash',
          purpose: 'Vec<u8>',
          amount: 'Balance',
          expiry: 'BlockNumber',
          status: 'u8',
        },
      },
    });

    return api.isConnected ? api : null;
  },

  /**
   * Format balance
   */
  toUnit: (balance, decimals) => {
    base = new BN(10).pow(new BN(decimals));
    dm = new BN(balance).divmod(base);
    return parseFloat(dm.div.toString() + '.' + dm.mod.toString());
  },

  /**
   * Check address
   */
  checkAddress: (address, prefix = 42) => {
    if (!crypto.checkAddress(address, prefix)?.[0]) {
      return new Error(
        'The address you entered is wrong. Please use one of the supported formats.',
      );
    }
    return false;
  },
};
