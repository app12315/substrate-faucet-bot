const formats = process.env.formats.split(',').map((x) => +x);

module.exports = formats;
