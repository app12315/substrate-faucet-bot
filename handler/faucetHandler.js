const faucet = require('../modules/faucet');
const substrateHelper = require('../utils/substrateHelper');
const { BN } = require('bn.js');
const db = require('../db/pgPool');
const moment = require('moment');

/**
 * Faucet messages are processed here
 *
 * @param {import('discord.js').Message} msg
 * @param {Array<String>} args
 */
module.exports = async (msg, args) => {
  if (!args.length) {
    return msg.reply('Insufficient parameters were specified');
  }

  const userId = msg.author.id;

  const lastCoinDateQuery = await db.query(
    `SELECT last_coins from users where user_id='${userId}'`,
  );

  if (lastCoinDateQuery?.rowCount) {
    const lastCoins = moment(lastCoinDateQuery.rows[0]?.last_coins);
    if (lastCoins.unix() > moment().unix()) {
      return msg.reply(
        `I have just sent you coins. Try again in ${moment
          .duration(lastCoins.diff())
          .humanize()}`,
      );
    }
  }

  const amount = 10;
  const transferAmount = new BN(amount).mul(new BN(10).pow(new BN(18)));

  const response = await faucet.send(args[0], transferAmount);

  if (response instanceof Error) {
    return msg.reply(response.message);
  }

  const newDate = moment().add(3, 'days').toISOString();
  if (lastCoinDateQuery?.rowCount === 0) {
    await db.query(
      `Insert into users (user_id,last_coins) values ('${userId}','${newDate}')`,
    );
    await db.query(
      `Insert into statics (user_id, use_count) values ('${userId}',0)`,
    );
  } else {
    await db.query(
      `Update users set last_coins='${newDate}' where user_id='${userId}'`,
    );

    const countQuery = await db.query(
      `SELECT use_count from statics where user_id='${userId}'`,
    );

    if (countQuery.rowCount) {
      await db.query(
        `Update statics set use_count='${
          countQuery.rows[0].use_count + 1
        }' where user_id='${userId}'`,
      );
    }
  }

  return msg.reply(
    `I have sent you ${substrateHelper.toUnit(
      transferAmount,
      18,
    )} coins here is the transaction hash: \`${response}\``,
  );
};
